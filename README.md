# GJS Babel Template

This template supports [Babel]() for transpiling ESNext features not yet available in GJS. By default it will transpile to code compatible with GJS 1.52.

## Features

Export Syntax

```js
export class Clazz {}
```

Decorators

```js
GObject.registerClass
export class GObjectClazz extends GObject {}
```

Class Properties (static too!)

```js
class Clazz {
    value = 5;
    static VALUE = 10;
}

log(new Clazz().value); // 5
log(Clazz.VALUE); // 10
```