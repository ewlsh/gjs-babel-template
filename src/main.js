/* main.js
 *
 * This is free and unencumbered software released into the public domain.
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * You should have received a copy of the Unlicense
 * along with this program.  If not, see <http://unlicense.org>.
 */

pkg.initGettext();
pkg.initFormat();
pkg.require({
  'Gio': '2.0',
  'Gtk': '3.0'
});

const { Gio, Gtk } = imports.gi;

const { GJSBabelTemplateWindow } = imports.window;

function main(argv) {
    const application = new Gtk.Application({
        application_id: 'org.gnome.GJSBabelTemplate',
        flags: Gio.ApplicationFlags.FLAGS_NONE,
    });

    application.connect('activate', app => {
        let activeWindow = app.activeWindow;
        
        if (!activeWindow) {
            activeWindow = new GJSBabelTemplateWindow(app);
        }

        activeWindow.present();
    });

    return application.run(argv);
}