/* window.js
 *
 * This is free and unencumbered software released into the public domain.
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * You should have received a copy of the Unlicense
 * along with this program.  If not, see <http://unlicense.org>.
 */

const { GObject, Gtk } = imports.gi;

@GObject.registerClass
export class GJSBabelTemplateWindow extends Gtk.ApplicationWindow {
    static [GObject.GTypeName] = 'GJSBabelTemplateWindow';
    static [Gtk.template] = 'resource:///org/gnome/GJSBabelTemplate/window.ui';
    static [Gtk.internalChildren] = ['label'];

    _init(application) {
        super._init({ application });
    }
}
